# Porgram launching instructions

* Open your favourite shell on a Windows or Unix based OS
* Go to your working directory with the `cd` command
* Type `python3 main.py` to start the program
* Choose the number of days you want to simulate the market and press Enter to start the simulation

# Goals
Simulate the price of the energy on a market. There are 4 differents instances.

## Home (Area)
    - population by area
    - Initial consumtion rate (10 kWh / house (with 2.19 people in each house))
    - Initial production rate ()
    - Initial policy among:
        - Always give to others
        - Always sell on the market
        - Sell if no takers

## Weather
    - Sun / Wind (2 times more efficient) / Temp coeff
    - Simulate weather (depend of differents areas ?)
        - Cold -> decrease temps -> increase energy consumption
        - Sunny -> increase energy production (% of Sun)
        - Windy -> increase energy production
    - Impact the price on the market
        - Less energy -> increase price

## External
    - Events that impact the price
    - Some personal examples
        - War 
        - Special Events (like football match)
        - Law

## Market 
    - multi-threaded for each transaction with homes
    - Initial price (0,145 euros / kWh)
    - prix = ((coeff_att*prix_j-1) * (1+coeff_elem_ext)) * (1 + coeff_echanges*somme(echanges))
    - echanges = coeff_vent*prod_vent + coeff_soleil*prod_soleil + coeff_temp*conso
        - 0.5 * prod_vent + 0.5 * prod_soleil + coeff_temp * conso = 0 

## TODO
    - 
