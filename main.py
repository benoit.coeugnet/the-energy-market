import concurrent.futures
import select
import multiprocessing
from multiprocessing import Process, Value, Array
import threading
import sysv_ipc
import os
import time
import random
# import keyboard
import signal
import sys
import socket

# ----------------------------- Declare processes ----------------------------- #
class Home(Process):
    def __init__(self, currentDay, sun_coeff, wind_coeff, temp_coeff, free_q, waiting_q, eD, hP, hN):
        super().__init__()
        self.day = currentDay
        self.prod_vent = 150
        self.prod_soleil = 75
        self.conso = 10
        self.sun_coeff = sun_coeff
        self.wind_coeff = wind_coeff
        self.temp = temp_coeff
        self.free_q = free_q
        self.waiting_q = waiting_q
        self.energyDelta = eD
        self.housePolicy = hP
        self.houseNumber = hN

    def run(self):
        HOST = "localhost"
        PORT = 1795
        waitingDay = 1
        while True:
            time.sleep(3)
            # print("House", self.houseNumber,"waiting for day",d[0],waitingDay)
            if waitingDay == self.day[0]:
                self.energyDelta = self.prod_vent * self.wind_coeff[self.houseNumber] + self.prod_soleil * self.sun_coeff[self.houseNumber] - self.conso * (30 - self.temp[self.houseNumber])
                didTransact = False

                # Global taking energy policy - Start #
                if self.energyDelta<0: # If house needs energy
                    self.waiting_q.put(1)  # On augmente la taille de la waiting queue de 1
                    time.sleep(1) # Wait until everyone declares that they need energy
                    while self.energyDelta<0 and not self.free_q.empty():
                        self.energyDelta += self.free_q.get()
                if self.energyDelta<0: # If house still needs energy
                    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
                        client_socket.connect((HOST, PORT))
                        client_socket.send((str(self.houseNumber) + str(self.energyDelta)).encode())
                        self.energyDelta=0
                        didTransact = True
                # Global taking energy policy - End #

                if self.housePolicy == 1:          
                    if self.energyDelta>0:
                        self.free_q.put(self.energyDelta)
                        self.energyDelta=0

                if self.housePolicy == 2:  # In the case they need to sell their energy
                    if self.energyDelta > 0:
                        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
                            client_socket.connect((HOST, PORT))
                            client_socket.send((str(self.houseNumber) + str(self.energyDelta)).encode())
                            self.energyDelta = 0
                            didTransact = True

                if self.housePolicy == 3:
                    if self.energyDelta>0:
                        if not self.waiting_q.empty(): # If takers
                            self.free_q.put(self.energyDelta)
                        else: # If no takers
                            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
                                client_socket.connect((HOST, PORT))
                                client_socket.send((str(self.houseNumber) + str(self.energyDelta)).encode())
                                didTransact = True
                        self.energyDelta = 0

                if not didTransact: # Dit au market que la maison n'a effectué aucune transaction ce jour
                    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
                        client_socket.connect((HOST, PORT))
                        client_socket.send((str(self.houseNumber) + "0").encode())

                # print("House",self.houseNumber,"has an energy delta of",self.energyDelta)
                waitingDay+=1

class Market(Process):
    def __init__(self, nbHouses, currentDay):
        super().__init__()
        self.signaux = [1,2,3,4]
        self.event = [0]*4
        self.serve = [True] * nbHouses
        self.price = 0.145
        self.transaction = [0] * nbHouses
        self.day = currentDay

    def run(self):
        waitingDay = 1
        # Initialize the signal handler for each event
        for sig in range(len(self.signaux)):
            signal.signal(self.signaux[sig], self.handler)

        # Transaction from the home to the market with sockets
        HOST = "localhost"
        PORT = 1795

        while True:
            if day[0] == waitingDay:
                for i in range(4):
                    self.serve[i] = True

                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
                    server_socket.bind((HOST, PORT))
                    server_socket.setblocking(False)
                    server_socket.listen(4)
                    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as pool:
                        while True in self.serve:
                            readable, writable, error = select.select([server_socket], [], [], 1)
                            if server_socket in readable:  # if server_socket is ready
                                client_socket, address = server_socket.accept()  # will return immediately
                                pool.submit(self.transactions, client_socket, address)


                # Initialize the event
                print("\n------------------- Event --------------------")
                processE = Process(target=self.external, args=())
                processE.start()
                processE.join()
                processE.terminate()

                # Impact of the events
                eventToday = False
                for events in range(len(self.event)):
                    if self.event[events] != 0:
                        self.price = round(self.price * (1 + self.event[events]/100),5)
                        self.event[events] = 0
                        eventToday = True
                if not eventToday:
                    print("No event this day")

                # Impact the exchange of energy between the houses and the market
                coeff_echanges = 0.0001
                print("\n------------------- Transactions -------------------")
                for house in range(len(self.transaction)):
                    if self.transaction[house] != 0:
                        print("Calcul of the price after the transaction with the house", house)
                        self.price = round(self.price * (1 - coeff_echanges * self.transaction[house]),5)
                print("\n-------------------- Price --------------------")
                print("Price of energy this day : ", self.price, "€/kWh")

                waitingDay += 1

    #  External event
    def handler(self, sig, frame):
        print("Event of the day", end=" : ")
        if sig == self.signaux[0]:
            self.event[0] = 25
            print("War (Price increase by 25%)")
        if sig == self.signaux[1]:
            self.event[1] = 10
            print("Snowstorm (Price increase by 10%)")
        if sig == self.signaux[2]:
            self.event[2] = -10
            print("Law (Price decrease by 10%)")
        if sig == self.signaux[3]:
            self.event[3] = 15
            print("Football WorldCup (Price increase by 15%)")

    # Transaction from the home to the market
    def transactions(self, s, a):
        with s:
            data = s.recv(1024)
            m = data.decode()
            energy = round(float(m[1:]),4)
            print("Transaction of " + str(energy) + " kWh from house " + m[0])
            self.transaction[int(m[0])] = energy
            self.serve[int(m[0])] = False

    # External event randomizer
    def external(self):
        randomN = random.randint(1, 1000000)
        if randomN%4 == 0:
            os.kill(os.getppid(),self.signaux[0])
        if randomN%5 == 0:
            os.kill(os.getppid(), self.signaux[1])
        if randomN%6 == 0:
            os.kill(os.getppid(), self.signaux[2])
        if randomN%7 == 0:
            os.kill(os.getppid(), self.signaux[3])


def weather(sun_coeff, wind_coeff, temp, Day):
    waitingDay = 1
    while True:
        if Day[0] == waitingDay:
            for i in range(N):
                sun_coeff[i] = round(random.random(),2)
                wind_coeff[i] = round(random.random(),2)
                if sun_coeff[i] > 0.5:
                    temp[i] = random.randint(20,40)
                else:
                    temp[i] = random.randint(0,20)
            waitingDay += 1


if __name__ == "__main__":
    # Initialisation of global variables - Start #
    N = 4
    P0 = 0.145
    housePolicies = [2,1,3,2]
    energyDelta = [0,0,0,0]
    # Initialisation of global variables - End #

    # Initialisation Shared Memory - Début #
    windy = Array('d',[0]*N)
    sunny = Array('d',[0]*N)
    temp = Array('d',[0]*N)
    day = Array('i',[0])
    # Initialisation Shared Memory - Fin #

    D = int(input("How many days do you want in your business world ?"))

    free_queue = multiprocessing.Queue(N)
    waiting_queue = multiprocessing.Queue(N)

    processW = Process(target=weather, args=(sunny,windy,temp, day))
    processW.start()

    pHomes = []
    for i in range(N):
        processH = Home(day, sunny, windy, temp, free_queue, waiting_queue,energyDelta[i],housePolicies[i],i)
        processH.start()
        pHomes.append(processH)


    time.sleep(1)
    print("-- Initialisation complete --")
    processM = Market(N, day)
    processM.start()
    while day[0] < D:
        day[0]+=1

        print("\n\n------------------------------ Day",day[0],": welcome back ------------------------------")
        time.sleep(5) # Waiting some time before tomorrow

        print("\n-------------------- Data --------------------")
        # Display everything that happened this day
        print("House policies :",housePolicies)

        # Displaying weather - Start #
        print("Sun coeff :",sunny[:])
        print("Wind coeff :",windy[:])
        print("Temp value :",temp[:])
        # Displaying weather - End #

        while not free_queue.empty(): # Reinitialising free queue
            free_queue.get()
        while not waiting_queue.empty(): # Reinitialising waiting queue
            waiting_queue.get()

        time.sleep(5)

    # Termination
    for i in range(N):
        pHomes[i].terminate()

    processW.terminate()
    processM.terminate()
    print("\n                               ----------------------")
    print("------------------------------ | Program terminated | ------------------------------")
    print("                               ----------------------")

